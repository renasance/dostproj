<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use Auth;

class AdminProfileController extends Controller
{
    // public function profile($name){
    // 	$user = User::whereName($name)->first();
    // 	return view('users.profile', compact('user'));
    // }
    /**
    * @param int $id
    * @return \Illuminate\Http\Response
    **/
    public function settings(){
    	$admin = Admin::find(Auth::admin()->id);
    	return view('admins.edit', compact('admin'));
    }

    public function update(Request $request, $id){
    	$admin = Admin::find($id);
    	// $user['name'] = $request->name;
    	// $user['password'] = bcrypt($request->password);
    	// $user['username'] = $request->username;
    	// $user['bdate'] = $request->bdate;

    	// $user->update();

    	$request->merge(array('password' => bcrypt($request->password)));
    	$admin->update($request->all());
    	return redirect('/admin');
    }
}
