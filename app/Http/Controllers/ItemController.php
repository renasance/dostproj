<?php

namespace App\Http\Controllers;

use App\Item;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;


class ItemController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $items = DB::table('items')->where('deleted', '=' ,'0')->where('office_id','=',$user = Auth::user()->id)->orderBy('category_id','id', 'asc')->get();
       return view('items.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cats = DB::table("categories")->get();
        return view('items.create', compact('cats'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $item = new Item;
        // $item->office_id = Auth::user()->id;
        // $item->category = $request->category;
        // $item->name = $request->name;
        // $item->condition = $request->condition;
        // $item->details = $request->details;
        // $item->save();
         Item::create($request->all());

        return redirect('/items');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $item = Item::find($id);
        return view('items.edit',compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Item::find($id);
        $item->update($request->all());

        return redirect('/items/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
    {
        $item = DB::table('items')->where('id', '=', $id)->update(['deleted' => '1']);
         // print_r($item);
        return redirect('/items');
        // $item = Item::find($id);
        // $item->delete();

        // return redirect('/items');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function archive()
    {
        $items=Item::where('deleted','=','1')->where('office_id','=',$user = Auth::user()->id)->orderBy('category_id', 'asc')->get();
        
        return view('items.archive', compact('items'));
    }

    public function archiveOffice()
    {
        $items=Item::where('deleted','=','1')->orderBy('category_id', 'asc')->get();
        
        return view('items.archiveFiles', compact('items'));
    }

}
