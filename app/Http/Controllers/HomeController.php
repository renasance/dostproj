<?php

namespace App\Http\Controllers;

use App\Item;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $items = DB::table('items')->where('deleted', '=' ,'0')->where('office_id','=',$user = Auth::user()->id)->orderBy('category_id','id', 'asc')->get();
       return view('items.index', compact('items'));
    }
}
