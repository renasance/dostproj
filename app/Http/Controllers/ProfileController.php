<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Auth;

class ProfileController extends Controller
{
    public function profile($name){
    	$user = User::whereName($name)->first();
    	return view('users.profile', compact('user'));
    }
    /**
    * @param int $id
    * @return \Illuminate\Http\Response
    **/
    public function settings(){
    	$user = User::find(Auth::user()->id);
    	return view('users.edit', compact('user'));
    }

    public function update(Request $request, $id){
    	$user = User::find($id);
    	// $user['name'] = $request->name;
    	// $user['password'] = bcrypt($request->password);
    	// $user['username'] = $request->username;
    	// $user['bdate'] = $request->bdate;

    	// $user->update();

    	$request->merge(array('password' => bcrypt($request->password)));
    	$user->update($request->all());
    	return redirect('/home');
    }

    public function viewAllProfile(){
       $users = DB::table('users')->orderBy('id', 'asc')->get();
       return view('users.viewAllProfile', compact('users'));
    }
}
