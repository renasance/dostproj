<?php

namespace App\Http\Controllers;

use App\ItemList;
use Illuminate\Http\Request;
use Auth;

class ItemListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('itemlists.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $item = new ItemList;
        // $item->office_id = Auth::user()->id;
        // $item->category = $request->category;
        // $item->name = $request->name;
        // $item->condition = $request->condition;
        // $item->details = $request->details;
        // $item->save();
         ItemList::create($request->all());

        return redirect('/itemlists');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $item = ItemList::find($id);
        return view('itemlists.edit',compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = ItemList::find($id);
        $item->update($request->all());

        return redirect('/itemlists/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = ItemList::find($id);
        $item->delete();

        return redirect('/itemlists');
    }
}
