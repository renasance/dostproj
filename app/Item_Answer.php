<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item_Answer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    protected $fillable = [
        'form_id', 'item_id', 'condition', 'details' 
    ];
}
