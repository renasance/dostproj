# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* OJT DOST Project
* 0.0.0.1 Beta

### How do I get set up? ###

* clone repo
* cd /location/of/your/repo/
* composer install && composer update
* Change env.example to env file. (If necessary change database, localhost, admin, password)
* php artisan key:generate
* php artisan serve