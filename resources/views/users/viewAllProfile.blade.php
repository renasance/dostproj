<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>Maintenance Checklist</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <style type="text/css">
        .successModal{
        font-size: 40px;
        text-align: center;
          }

      .add{
        font-size:15px;
      }

      .add1{
        padding-left: 12px;
      }
      body{
        font-family: 'Roboto', sans-serif;
        font-weight:100;
      }

      .page-header{
        padding-top: 30px;

      }

      .dropwdown{
        width: 100%;

      }

      .dropdown-toggle{
        width: 100%;

      }
      .dropdown-menu{
        width: 100%;

      }
      .panel-heading p{
      	color: black
      }
      /*body{
          position: fixed;
          width: 100%;
          height: 100%;
                
          background-size: cover;
               
          background-color: #a8dbdf;
      }*/
     
    </style>
</head>
<body>
	<nav class="navbar navbar-inverse navbar-static-top">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

                    <!-- Branding Image -->
                <a class="navbar-brand" href="/home">
                  Maintenance Checklist
                </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                &nbsp;
            </ul>
             <!-- Right Side Of Navbar -->
             <ul class="nav navbar-nav navbar-right">
                 <!-- Authentication Links -->
                
                     <li class="dropdown">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Admin<span class="caret"></span>
                         </a>
                        	<ul class="dropdown-menu" role="menu">
       
                                <li><a href="/admin/home">Home</a></li>
                          </ul>
                     </li>
                 
             </ul>
         </div>
    </nav>
    <div class="container">
    	<div class="row">
			<div class="col-md-8 col-md-offset-2">
				@forelse($users as $user)
				<div class="panel panel-default">
					<div class="panel-heading">
						<span>Offices</span>
					</div>
					<div class="panel-body panel-default">
							{{ $user->id }}. &nbsp;
							{{ $user->name }}
							<form method="get" action="get">
								<a class="btn btn-primary pull-right" href="{{url('/user/settings')}}"><span class="glyphicon glyphicon-pencil"></span>Edit</a>
							</form> 
							
					</div>
				</div>
				@empty
					No results found
				@endforelse
			</div>
		</div>
    </div>
		
</body>
</html>