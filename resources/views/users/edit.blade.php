<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>{{ Auth::user()->name }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
        <style type="text/css">
            @import url(https://fonts.googleapis.com/css?family=Roboto:400,300,100,700,500);
            .search{
                text-align: center;
            }

            .admin{
                padding-left: 50px;
            }
            .toolbarRight{
                padding-right: 20px;
            }
            .editProfile{
                padding-top: 70px;
            }
            body{
                font-family: 'Roboto', sans-serif;
                font-weight:325;
            }
        </style>
    </head>
    <body>
       <nav class="navbar navbar-inverse navbar-static-top">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                      Maintenance Checklist
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register Office</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Admin <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <!-- <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li> -->
                                    <!-- <li><a href="/profile/{{ Auth::user()->name }}">My profile</a></li> -->
                                    <!-- <li><a href="/user/settings">Settings</a></li> -->
                                    <li><a href="/admin/home">Home</a></li>
                                    <!-- <li><a href="/itemlists/create">Add Items</a></li> -->

                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
        </nav>
    <div class="container">
    <p class="editProfile" style="font-size:30px;">Edit Profile</p>
    <hr>
    <div class="row">
      <div class="col-md-12 personal-info jumbotron">
        
        <form class="form-horizontal" role="form" action="/user/settings/{{ $user->id}} " method="POST">
            {{ csrf_field() }}
            {{ method_field('put') }}
          <input type="hidden" name="id" value="{{ Auth::user()->id }}" />&nbsp;
          <div class="form-group">
            <p class="col-lg-3 control-label">Office Name:</p>
            <div class="col-lg-8">
              <input type="text" name="name" class="form-control" value="{{ $user->name }}" style="width:500px;"/>
            </div>
          </div>
          <div class="form-group">
            <p class="col-md-3 control-label">Password:</p>
            <div class="col-md-8">
              <input type="password" name="password" class="form-control" value="{{ $user->password }}" style="width:500px;">
            </div>
          </div>
          <!-- <div class="form-group">
            <p class="col-md-3 control-label">Confirm password:</p>
            <div class="col-md-8">
              <input class="form-control" type="password"  style="width:500px;">
            </div>
          </div> -->
          <div class="form-group">
            <div class="col-md-8 col-md-offset-4">
              <input type="submit" class="btn btn-success" value="Save Changes" />
              <a href="{{url('/home')}}" type="reset" class="btn btn-default">Cancel</a>
            </div>
          </div>
        </form>
      </div>
  </div>
</div>
<!-- <div id="editModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    Modal Content
    <div class="modal-content">
      <div class="modal-body">
        <div class="glyphicon glyphicon-ok-circle successModal">
            <div class="text-center modalContent">Profile Saved!</div>
            </div>
        </div>
        
      <div class="modal-footer">
        <a href="{{url('/home')}}" type="button" class="btn btn-default">Close</a>
      </div>
    </div>

  </div>
</div> -->
</body>
</html>

