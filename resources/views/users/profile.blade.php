@extends('layouts.app')
<style type="text/css">
	.panel-footer{
		height:60px;
	}

</style>


@section('content')
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="panel panel-default">
				<div class="panel-body ">
					<h4>Name of Office: {{ Auth::user()->name }} </h4>
					<h4>Password: <input type="hidden" value="{{ Auth::user()->password }}"/></h4>
				</div>
				<div class="panel-footer">
					<a href="/user/settings" class="btn btn-success"> Edit </a>
				</div>
			</div>
		</div>
	</div>	
@endsection