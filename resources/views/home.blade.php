<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>Maintenance Checklist</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        .successModal{
        font-size: 40px;
        text-align: center;
          }

      .add{
        font-size:15px;
      }

      .add1{

        padding-left: 12px;
      }
      body{
        font-family: 'Roboto', sans-serif;
        font-weight:300;
      }

      .page-header{
        padding-top: 30px;

      }

      .dropwdown{
        width: 100%;

      }

      .dropdown-toggle{
        width: 100%;

      }
      .dropdown-menu{
        width: 100%;

      }
    </style>
</head>
<body>
<nav class="navbar navbar-inverse navbar-static-top">
          
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                      Maintenance Checklist
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <!-- <li><a href="{{ route('register') }}">Register Office</a></li> -->
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                    <!-- <li><a href="/profile/{{ Auth::user()->name }}">My profile</a></li> -->
                                    <!-- <li><a href="/user/settings">Settings</a></li> -->
                                    <!-- <li><a href="/categories/create">Add Categories</a></li> -->
                                    <!-- <li><a href="/home">Home</a></li> -->
                                    <!-- <li><a href="/items/create">Add Items</a></li> -->

                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
        </nav>

<div class="page-header text-center">
  <h1 class="">Maintenance Forms</h1>

</div>

<div class="container">
<div class="jumbotron center maintenance">
    <div class="container">
        <div class="row center-block">
          <form class="form-horizontal" role="form" action="#" method="POST">
            <div class="column">
                  <div class="row">
                    <div class="pull-right">
                      <a href="{{url('/items/create')}}" class="btn btn-info glyphicon glyphicon-plus"> Add </a>
                      <!-- <a href="{{url('/items/create')}}" class="btn btn-info glyphicon glyphicon-edit"> Edit </a> -->
                  </div>
                  </div>
                  <div class="row">
                    <div class="col-md-2"><b>Item code</b></div>
                    <div class="col-md-3"><b>In Good Condition?</b></div>
                    <div class="col-md-3 col-md-offset-1"><b>Details</b></div>
                    <div class="col-md-3"></div>
                  </div>
                
               
                
                   @forelse($items as $item)
                <div class="row">
                    <div class="col-md-2">
                      {{ $item->code}}
                    </div>
                  
                    <div class="col-md-3">
                      <div class="form-group">
                        <label class="radio-inline"><input type="radio" name="optradio1" disabled="disabled">Yes</label>
                        <label class="radio-inline"><input type="radio" name="optradio1" disabled="disabled">No</label>
                      </div>
                    </div>

                    <div class="col-md-4 col-offset-2">
                        <input type="text" class="form-control" id="usr" name="details" value="{{ $item->reason}}">
                    </div>

                    <div class="col-md-1">
                      <span class="glyphicon glyphicon-trash"></span>
                    </div>
                </div>
                  
                  <!-- {{ $item->category_id }}
                  {{ $item->office_id }}
                  {{ $item->deleted }}
                  {{ $item->reason }} -->
              
              @empty
                No results found
              @endforelse
            </div>
            <div class="row">
              <div class="pull-right">
               <button type="submit" class="btn btn-success glyphicon glyphicon-ok-circle" data-toggle="modal" data-target="#addModal">Submit</button>
                 <!--<button type="button" class="btn btn-danger glyphicon glyphicon-remove-circle">Clear</button>-->
              </div>
            </div>
          </form>
        </div>
    </div>
</div>
</div>

<div id="addModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Success!</h4>
      </div>
      <div class="modal-body">
        <div class="glyphicon glyphicon-ok-circle successModal">
            <div class="text-center modalContent">
                Alright! You've updated your list!</div>
            </div>
        </div>
        
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add Item</h4>
      </div>
      <div class="modal-body">
            <div class="form-group">
              <label for="usr">Item Name:</label>
              <input type="text" class="form-control" id="usr">
              <label for="usr">Category:a</label>
              <div class="form-group">
                    <div class="dropdown">
                      <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Item Type
                      <span class="caret"></span></button>
                      <ul class="dropdown-menu">
                        <li><a href="#" class="active">Structure</a></li>
                        <li><a href="#">Furniture</a></li>
                        <li><a href="#">Equipment</a></li>
                      </ul>
                    </div>
                  </div>
            </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success glyphicon glyphicon-ok" action="submit">Submit</button>
        <button type="button" class="btn btn-default glyphicon" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- include javascript, jQuery FIRST -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
</body>

</html>
