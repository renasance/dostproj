<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>Maintenance Checklist</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <style type="text/css">
        .successModal{
        font-size: 40px;
        text-align: center;
          }

      .add{
        font-size:15px;
      }

      .add1{
        padding-left: 12px;
      }
      body{
        font-family: 'Roboto', sans-serif;
        font-weight:100;
      }

      .page-header{
        padding-top: 30px;

      }

      .dropwdown{
        width: 100%;

      }

      .dropdown-toggle{
        width: 100%;

      }
      .dropdown-menu{
        width: 100%;

      }
     /*.form-horizontal{
        background-color: ghostwhite;
     }
     .container{
            background-color: white;
    }*/
    </style>
</head>
<body>
 <nav class="navbar navbar-inverse navbar-static-top">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="/home">
                      Maintenance Checklist
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                           <!--  <li><a href="{{ route('register') }}">Register Office</a></li> -->
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                    <!-- <li><a href="/profile/{{ Auth::user()->name }}">My profile</a></li>
                                    <li><a href="/user/settings">Settings</a></li>
                                    <li><a href="/home">Home</a></li>
                                    <li><a href="/itemlists/create">Add Items</a></li> -->

                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
        </nav>
<div class="page-header text-center">
  <h1 class="">Maintenance Forms</h1>

</div>

<div class="container">
<div class="jumbotron center maintenance">
    <div class="container">
         <!-- <form class="form-horizontal" role="form" action="#" method="POST"> -->
           <div class="row center-block"> 
            <div class="column">
                  <div class="row">
                    <!-- <div class="col-md-5 col-md-offset-9" id="date">
                        <b>Date Accomplished:</b> <input type="date" name="date"></br>
                    </div> -->
                    <div class=" pull-right">
                       <a href="{{url('/items/archive')}}" class="btn btn-info"><span class="glyphicon glyphicon-briefcase"> Archive </span> </a>
                       <a href="{{url('/items/create')}}" class="btn btn-info"><span class="glyphicon glyphicon-plus-sign"> Add </span> </a>
                      <!-- <a href="{{url('/items/create')}}" class="btn btn-info glyphicon glyphicon-edit"> Edit </a> -->
                  </div>
                  </div>
                  <div class="row">
                    <div class="col-md-2"><b>Item code</b></div>
                    <div class="col-md-3"><b>In Good Condition?</b></div>
                    <div class="col-md-3 col-md-offset-1"><b>Details</b></div>
                    <div class="col-md-3"></div>
                  </div>
                
               
                
                   @forelse($items as $item)
                <div class="row">
                    <input type="hidden" name="item_id" value="{{ $item->code}}">
                    <input type="hidden" name="office_id" value="{{ Auth::user()->id }}">
                    <div class="col-md-2">
                      {{ $item->code}}
                    </div>
                  
                    <div class="col-md-3">
                      <div class="form-group">
                        <label class="radio-inline"><input type="radio" name="condition" value="1">Yes</label>
                        <label class="radio-inline"><input type="radio" name="condition" value="0">No</label>
                      </div>
                    </div>

                    <div class="col-md-4 col-offset-2">
                        <input type="text" class="form-control" id="usr" name="details">
                    </div>

                    <div >
                     <form class="form-delete" action="/items/{{ $item->id }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <input type="hidden" value="{{ $item->id }}"/>
                        <input type="submit" class="btn btn-danger" value="X" />
                        
                    </form>
                      
                    </div>
                </div>
                  
                  <!-- {{ $item->category_id }}
                  {{ $item->office_id }}
                  {{ $item->deleted }}
                  {{ $item->reason }} -->
              
              @empty
                No results found
              @endforelse
            </div>
            <div class="row">
              <div class=" pull-right">
               <button type="submit" class="btn btn-success " data-toggle="modal" data-target="#addModal"> <span  class="glyphicon glyphicon-send"> Submit </span></button>
                 <!--<button type="button" class="btn btn-danger glyphicon glyphicon-remove-circle">Clear</button>-->
              </div>
            </div>
          <!-- </form> -->
        </div>
    </div>
</div>
</div>

<div id="addModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Success!</h4>
      </div>
      <div class="modal-body">
        <div class="glyphicon glyphicon-ok-circle successModal">
            <div class="text-center modalContent">
                Alright! You've updated your list!</div>
            </div>
        </div>
        
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
        
            function getConfirmation(){
               var retVal = confirm("Do you want to continue ?");
               if( retVal == true ){
                  document.write ("User wants to continue!");
                  return true;
               }
               else{
                  document.write ("User does not want to continue!");
                  return false;
               }
            }
         
      </script>
</body>

