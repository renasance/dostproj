@extends('layouts.app')


@section('content')
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel-body panel-default">
				<form action="/items/{{ $item->id }}" method="POST">
					{{ csrf_field() }}
					{{ method_field('PUT') }}

                        <input type="hidden" name="office_id" value="{{ Auth::user()->id }}" />

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name" class="col-md-4 control-label">Name</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                        <div class="form-group{{ $errors->has('cat_id') ? ' has-error' : '' }}">
                            <label for="category" class="col-md-4 control-label">Category</label>
                                <div class="col-md-6">
                                            
                                    <select id="cat_id" class="form-control" name="cat_id" required autofocus>

                                        @foreach ($cats as $cat)
                                            <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                        @endforeach
                                        </select>

                                            @if ($errors->has('cat_id'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('cat_id') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                        <input type="submit" class="btn btn-success pull-right" value="Add" />
                      </form>
				</div>
			</div>
		</div>
@endsection