<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>Add Items</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
        <style type="text/css">
            @import url(https://fonts.googleapis.com/css?family=Roboto:400,300,100,700,500);
            .search{
                text-align: center;
            }

            .admin{
                padding-left: 50px;
            }
            .toolbarRight{
                padding-right: 20px;
            }
            .addItems{
                padding-top: 70px;
            }
            body{
                font-family: 'Roboto', sans-serif;
                font-weight:325;
            }
        </style>
    </head>
    <body>
       <nav class="navbar navbar-inverse navbar-static-top">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="/home">
                      Maintenance Checklist
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                           <!--  <li><a href="{{ route('register') }}">Register Office</a></li> -->
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/items">Home</a></li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
        </nav>
    <div class="container">
    <p class="addItems" style="font-size:30px;">Add Items</p>
    <hr>
    <div class="row">
      <div class="col-md-12 personal-info jumbotron">
        
        <form class="form-horizontal" role="form" action="/items" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="office_id" value="{{ Auth::user()->id }}" />
            <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                <p class="col-lg-3 control-label">Item Code</p>    

                <div class="col-md-8">
                <input id="code" type="text" class="form-control" name="code" value="{{ old('code') }}" style="width:500px;" required autofocus>

                @if ($errors->has('code'))
                    <span class="help-block">
                      <strong>{{ $errors->first('code') }}</strong>
                    </span>
                @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                <p class="col-lg-3 control-label">Category</p> 
                    <div class="col-md-8">
                        <select id="category_id" class="form-control" name="category_id" style="width:500px;" required autofocus>
                            <option value="">--- Select Category ---</option>
                            @foreach ($cats as $cat)
                                <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('category_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('category_id') }}</strong>
                            </span>
                        @endif
                    </div>
            </div>
         
                <input id="deleted" type="hidden" name="deleted" value="0" />
                <input id="reason" type="hidden" name="reason" value="null" />
                <input type="submit" class="btn btn-success pull-right" value="Add" />
          </form>
      </div>
  </div>
</div>
</body>
</html>

